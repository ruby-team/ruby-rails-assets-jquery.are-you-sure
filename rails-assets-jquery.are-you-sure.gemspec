# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-jquery.are-you-sure/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-jquery.are-you-sure"
  spec.version       = RailsAssetsJqueryAreYouSure::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "A light-weight jQuery 'dirty forms' Plugin - it monitors html forms and alerts users to unsaved changes if they attempt to close the browser or navigate away from the page. (Are you sure?)"
  spec.summary       = "A light-weight jQuery 'dirty forms' Plugin - it monitors html forms and alerts users to unsaved changes if they attempt to close the browser or navigate away from the page. (Are you sure?)"
  spec.homepage      = "https://github.com/codedance/jquery.AreYouSure"
  spec.license       = "MIT/GPLv2"

  spec.files         = `find ./* -type f | cut -b 3-`.split($/)
  spec.require_paths = ["lib"]

  spec.add_dependency "rails-assets-jquery", ">= 1.4.2"
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
